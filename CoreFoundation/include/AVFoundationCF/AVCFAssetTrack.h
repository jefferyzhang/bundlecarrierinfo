/*
	File:		AVCFAssetTrack.h
	
	Framework:	AVFoundationCF
	
    (C)  Copyright 2013  Apple Inc.  All rights reserved.

    You may only use this file to build WebKit and for no other purpose.

    THIS SOFTWARE IS PROVIDED BY APPLE INC. "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
    INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. BE LIABLE FOR ANY 
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef AVCFASSETTRACK_H
#define AVCFASSETTRACK_H

#include <AVFoundationCF/AVCFBase.h>
#include <AVFoundationCF/AVCFAsynchronousPropertyValueLoading.h>

#include <CoreMedia/CMTime.h>
#include <CoreMedia/CMTimeRange.h>

#include <CoreGraphics/CGGeometry.h>
#include <CoreGraphics/CGAffineTransform.h>

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push, 4)

typedef struct OpaqueAVCFAsset *AVCFAssetRef;
typedef struct OpaqueAVCFAssetTrack *AVCFAssetTrackRef;

AVCF_EXPORT CFStringRef AVCFAssetTrackGetMediaType(AVCFAssetTrackRef assetTrack);
AVCF_EXPORT int64_t AVCFAssetTrackGetTotalSampleDataLength(AVCFAssetTrackRef assetTrack);

AVCF_EXPORT CFStringRef AVCFAssetTrackCopyLanguageCode(AVCFAssetTrackRef assetTrack);
AVCF_EXPORT CFStringRef AVCFAssetTrackCopyExtendedLanguageTag(AVCFAssetTrackRef assetTrack);
AVCF_EXPORT CGSize AVCFAssetTrackGetNaturalSize(AVCFAssetTrackRef assetTrack);
AVCF_EXPORT CGAffineTransform AVCFAssetTrackGetPreferredTransform(AVCFAssetTrackRef assetTrack);
AVCF_EXPORT CFArrayRef AVCFAssetTrackCopyCommonMetadata(AVCFAssetTrackRef assetTrack);

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif // AVCFASSETTRACK_H
