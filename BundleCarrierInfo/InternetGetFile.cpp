#include "stdafx.h"
#include "BundleCarrier.h"

#include <msclr\marshal.h>

using namespace System;
using namespace System::Net;
using namespace System::IO;
using namespace msclr::interop;
using namespace System::Collections;

void logIt(String^ s, bool outputStdout, bool outputDebugger)
{
	String^ tag ="BundleInfo";
	if (outputStdout) System::Console::WriteLine(s);
	if (outputDebugger)
	{
		System::Diagnostics::Trace::WriteLine(s);
		if (!String::IsNullOrEmpty(tag))
		{
			System::Diagnostics::Trace::WriteLine(String::Format("[{0}]: {1}", tag,s));
		}
	}
}


void GetVersionXml()
{
	HttpWebRequest^ request = safe_cast<HttpWebRequest^>(HttpWebRequest::Create("http://ax.itunes.apple.com/check/version/cache/version.xml"));
	request->ServicePoint->Expect100Continue = false;
	request->AutomaticDecompression = DecompressionMethods::GZip | DecompressionMethods::Deflate;
	
	WebResponse^ res = safe_cast<WebResponse^>(request->GetResponse());
	String^ responseStr = "";
	try
	{
		StreamReader^ reader = gcnew StreamReader(res->GetResponseStream());
		responseStr = reader->ReadToEnd();
		marshal_context context;
		const char* pcszXML = context.marshal_as<const char*>(responseStr);
		//PlistFileParser(pcszXML, responseStr->Length);
		PlistFileToCarrierFile(pcszXML, responseStr->Length);
	}
	finally
	{
		res->Close();
	}

}

String^ GetDownLoadFileName(String^ surl)
{
	return surl->Substring(surl->LastIndexOf('/') + 1);
}

void DownloadFile(const char* surl1, char* sPath1)
{
	String^ surl = gcnew String(surl1);
	String^ sPath = gcnew String(sPath1);
	WebClient^ myWebClient = gcnew WebClient;
	// Download the Web resource and save it into the current filesystem folder.
	extern wchar_t sDownLoadFolder[MAX_PATH];
	String^ dstPath = Path::Combine(gcnew String(sDownLoadFolder), sPath);
	if(!Directory::Exists(dstPath))
	{
		Directory::CreateDirectory(dstPath);
	}
	String^ filename = Path::Combine(dstPath, GetDownLoadFileName(surl));
	myWebClient->DownloadFile(surl, filename);

}

int RunExe(String^ sIPCC, String^ sPath)
{
	int ret = ERROR_NOT_FOUND;
	if (Directory::Exists(sPath))
	{
		try
		{
			Directory::Delete(sPath);
		}catch(...)
		{

		}
	}

	String^ unzip_file;// = Path::Combine(System::Environment::GetEnvironmentVariable("APSTHOME"), "7z.exe");
	extern wchar_t s7zFolder[MAX_PATH];
	unzip_file = Path::Combine(gcnew String(s7zFolder), "7z.exe");
	if (System::IO::File::Exists(unzip_file))
	{
		System::Diagnostics::Process^ p = gcnew System::Diagnostics::Process();
		p->StartInfo->FileName = unzip_file;
		p->StartInfo->Arguments = String::Format("x \"{0}\" -o\"{1}\" -aoa -r", sIPCC, sPath);
		p->StartInfo->CreateNoWindow = true;
		p->StartInfo->WindowStyle = System::Diagnostics::ProcessWindowStyle::Hidden;
		p->StartInfo->UseShellExecute = false;
		p->Start();
		p->WaitForExit(15 * 60 * 1000);
		ret = p->ExitCode;
	}
	return ret;
}

void QueryInfoFile(String^ sPathRoot)
{
	extern wchar_t sIniFileName[MAX_PATH];
	array<String^>^subdirectoryEntries = Directory::GetDirectories( sPathRoot );
	IEnumerator^ dirs = subdirectoryEntries->GetEnumerator();
	while ( dirs->MoveNext() )
	{
		String^ subdirectory = safe_cast<String^>(dirs->Current);
		String^ subKey = subdirectory->Substring(subdirectory->LastIndexOf('\\') + 1);
		logIt(subdirectory, true, true);
		array<String^>^ fileEntries = Directory::GetFiles(subdirectory, "*.ipcc", SearchOption::AllDirectories);
		IEnumerator^ files = fileEntries->GetEnumerator();
		String^ filename;
		while(files->MoveNext())
		{
			filename = safe_cast<String^>(files->Current);
			logIt(filename, true, true);
		}
		if (System::IO::File::Exists(filename))
		{
			String^ sRooPa = Path::Combine(System::Environment::GetEnvironmentVariable("TEMP"),"fdipccuntract");
			RunExe(filename, sRooPa);

			String^ carrierInfo = Path::Combine(sRooPa,String::Format("Payload\\{0}.bundle",subKey),"Info.plist");
			if (System::IO::File::Exists(carrierInfo))
			{
				//getCarrierInfo;
				marshal_context context;
				const char* pfilename = context.marshal_as<const char*>(carrierInfo);
				std::string s = GetPlistInfoName(pfilename);
				if (!s.empty())
				{
					logIt(String::Format("[Info=]{0}", gcnew String(s.c_str())), true, true);
				}
				else
					logIt(carrierInfo, true, true);
			}
			else
			{
				logIt(String::Format("{0} file not found!!", subKey), true, true);
			}
		}
	}

}

void QueryInFile(String^ sPathRoot)
{
	extern wchar_t sIniFileName[MAX_PATH];
	array<String^>^subdirectoryEntries = Directory::GetDirectories( sPathRoot );
	IEnumerator^ dirs = subdirectoryEntries->GetEnumerator();
	while ( dirs->MoveNext() )
	{
		String^ subdirectory = safe_cast<String^>(dirs->Current);
		String^ subKey = subdirectory->Substring(subdirectory->LastIndexOf('\\') + 1);
		logIt(subdirectory, true, true);
		array<String^>^ fileEntries = Directory::GetFiles(subdirectory, "*.ipcc", SearchOption::AllDirectories);
		IEnumerator^ files = fileEntries->GetEnumerator();
		String^ filename;
		while(files->MoveNext())
		{
			filename = safe_cast<String^>(files->Current);
			logIt(filename, true, true);
		}
		if (System::IO::File::Exists(filename))
		{
			String^ sRooPa = Path::Combine(System::Environment::GetEnvironmentVariable("TEMP"),"fdipccuntract");
			RunExe(filename, sRooPa);

			String^ carrierInfo = Path::Combine(sRooPa,String::Format("Payload\\{0}.bundle",subKey),"carrier.plist");
			if (System::IO::File::Exists(carrierInfo))
			{
				//getCarrierInfo;
				marshal_context context;
				const char* pfilename = context.marshal_as<const char*>(carrierInfo);
				std::string s = GetPlistCarriarName(pfilename);
				if (!s.empty())
				{
					const wchar_t* psubKey = context.marshal_as<const wchar_t*>(subKey);
					WritePrivateProfileString(_T("carrierName"), psubKey, UTF16FromUTF8(s.c_str()).c_str(),sIniFileName);
				}
			}
			else
			{
				logIt(String::Format("{0} file not found!!", subKey), true, true);
			}
		}
	}

}