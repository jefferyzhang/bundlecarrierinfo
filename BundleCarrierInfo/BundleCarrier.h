#pragma once

#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CFError.h>
#include <CoreFoundation/CFDictionary.h>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation/CFData.h>
#include <CoreFoundation/CFPropertyList.h>
#include <CoreFoundation/CFURLAccess.h>
//#include <CoreFoundation/CoreFoundation.h>

void logIt(System::String^ s, BOOL outputStdout=true, BOOL outputDebugger=true);



void PlistFileParser(const char *plst, int nLen);
void PlistFileToCarrierFile(const char *plst, int nLen);
void GetXMLBundleIPCCFiles();
std::string GetPlistCarriarName(const char* fileName);
std::string GetPlistInfoName(const char* fileName);
std::wstring UTF16FromUTF8(const char * utf8) ;

void GetVersionXml();
void DownloadFile(const char* surl, char* sPath);
System::String^ GetDownLoadFileName(System::String^ surl);
int RunExe(System::String^ sIPCC, System::String^ sPath);
void QueryInFile(System::String^ sPathRoot);
void QueryInfoFile(System::String^ sPathRoot);
