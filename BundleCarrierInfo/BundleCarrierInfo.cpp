// BundleCarrierInfo.cpp : main project file.

#include "stdafx.h"
#include "BundleCarrier.h"
using namespace System;
using namespace System::IO;
using namespace Microsoft::Win32;

void SetRuntime()
{
	FileInfo^ iTunesMobileDeviceFile;
	DirectoryInfo^ ApplicationSupportDirectory;
	if (System::Environment::Is64BitOperatingSystem)
	{
		String^ dir1 = Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Mobile Device Support","InstallDir","iTunesMobileDevice.dll")->ToString() + "iTunesMobileDevice.dll";
		iTunesMobileDeviceFile = gcnew FileInfo(dir1);
		String^ dir2 = Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Apple Inc.\\Apple Application Support", "InstallDir", Environment::CurrentDirectory)->ToString();
		ApplicationSupportDirectory = gcnew DirectoryInfo(dir2);
	}
	else
	{   
		String^ ss=(String^)Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support\\Shared", "iTunesMobileDeviceDLL", "iTunesMobileDevice.dll");
		iTunesMobileDeviceFile = gcnew FileInfo(ss);
		ss = (String^)Registry::GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Application Support", "InstallDir", Environment::CurrentDirectory);
		ApplicationSupportDirectory = gcnew DirectoryInfo(ss);
	}

	String^ directoryName = iTunesMobileDeviceFile->DirectoryName;
	if (!iTunesMobileDeviceFile->Exists)
	{
		//throw new FileNotFoundException("Could not find iTunesMobileDevice file");
	}
	//gcnew array<String^>
	array<String^>^ strings  = {directoryName, ApplicationSupportDirectory->FullName, Environment::GetEnvironmentVariable("Path") };
	System::Environment::SetEnvironmentVariable("Path", String::Join(";", strings));

}

wchar_t  sDownLoadFolder[MAX_PATH] = _T("J:\\temp\\IPCC");
wchar_t  s7zFolder[MAX_PATH] = _T("J:\\temp");
wchar_t sIniFileName[MAX_PATH] = _T("J:\\temp\\CarrierBundle.ini");
BOOL  bDownLoad = true;
int main(array<System::String ^> ^args)
{
	//SetRuntime();
	if (bDownLoad)
	{
		GetVersionXml();
	}
	//QueryInfoFile(gcnew String(sDownLoadFolder));
    return 0;
}
