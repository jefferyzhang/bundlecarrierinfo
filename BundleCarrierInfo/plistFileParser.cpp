#include "stdafx.h"
#include "BundleCarrier.h"

#include <msclr\marshal.h>

using namespace std;

using namespace System;

void logItC(TCHAR* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	TCHAR s[1024];
	_vstprintf_s(s, fmt, args);
	TCHAR s1[1024];
	_stprintf_s(s1, 1024, _T("[%s]: %s"), _T("BundleInfo"), s);
	_tprintf(s);
	OutputDebugString(s1);
	va_end(args);
}

typedef struct __tagInfoCarrier
{
public:
	string bundleVersion;
	string sBundleURL;
}InfoCarrier;

class CarrierConfig
{
public:
	map<string, InfoCarrier> m_gernalInfo;//key is 	string osVersion;
	map<string, InfoCarrier> m_iPadInfo;//key is 	string osVersion;
	map<string, InfoCarrier> m_iPhoneInfo;//key is 	string osVersion;
	map<string, map<string, InfoCarrier>> m_byproducttype;

	void addGernalInfo(string sVer, string bundleVer, string sBundleUrl)
	{
		InfoCarrier in;
		in.bundleVersion = bundleVer;
		in.sBundleURL = sBundleUrl;
		m_gernalInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}
	void addGernalInfo(string sVer, InfoCarrier in)
	{
		m_gernalInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}
	void addiPadInfo(string sVer, string bundleVer, string sBundleUrl)
	{
		InfoCarrier in;
		in.bundleVersion = bundleVer;
		in.sBundleURL = sBundleUrl;
		m_iPadInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}
	void addiPadInfo(string sVer, InfoCarrier in)
	{
		m_iPadInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}
	void addiPhoneInfo(string sVer, string bundleVer, string sBundleUrl)
	{
		InfoCarrier in;
		in.bundleVersion = bundleVer;
		in.sBundleURL = sBundleUrl;
		m_iPhoneInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}
	void addiPhoneInfo(string sVer, InfoCarrier in)
	{
		m_iPhoneInfo.insert(pair<string,InfoCarrier>(sVer,in));
	}

	void addProductInfo(string sProduct,string sVer, InfoCarrier in)
	{
		//m_iPhoneInfo.insert(pair<string,InfoCarrier>(sVer,in));
		if(m_byproducttype.find(sProduct)!=m_byproducttype.end())
		{
			m_byproducttype.at(sProduct).insert(pair<string,InfoCarrier>(sVer,in));
		}
		else
		{
			map<string, InfoCarrier> a;
			a.insert(pair<string,InfoCarrier>(sVer,in));
			m_byproducttype.insert(pair<string,map<string,InfoCarrier>>(sProduct, a));
		}
	}

};

std::map<std::string,CarrierConfig> g_carrierbunders;

std::string cfstr_2_cstr_utf8(CFStringRef cfstr) {
	std::string  ret;
	CFIndex cstr_size = CFStringGetMaximumSizeForEncoding(CFStringGetLength(cfstr), kCFStringEncodingUTF8) + 1;
	char *cstr = (char *)malloc(cstr_size);
	if (cstr == NULL){
		logItC(_T("failed to allocate to CFStringRef to C string conversion"));
		return ret;
	}

	if (!CFStringGetCString(cfstr, cstr, cstr_size, kCFStringEncodingUTF8)) {
		logItC(_T("failed to convert CFStringRef to C string"));
		free(cstr);
		return ret;
	}
	if(cstr != NULL)
	{
		ret = cstr;
		free(cstr);
	}

	return ret;
}

BOOL ReadBundleInfo(CFDictionaryRef dict, InfoCarrier &infocar)
{
	BOOL bRet = FALSE;
	if (CFDictionaryContainsKey(dict, CFSTR("BundleURL")))
	{
		infocar.sBundleURL = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue(dict, CFSTR("BundleURL")));
		bRet = TRUE;
	}
	if (CFDictionaryContainsKey(dict, CFSTR("BuildVersion")))
	{
		infocar.bundleVersion = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue(dict, CFSTR("BuildVersion")));
	}

	return bRet;
}

void ParserByProductType_v1(CFDictionaryRef dictRef, CarrierConfig &ccfig)
{
	CFIndex size = CFDictionaryGetCount(dictRef);
	CFTypeRef *keysTypeRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
	CFTypeRef *valuesRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
	if (keysTypeRef && valuesRef)
	{
		CFDictionaryGetKeysAndValues(dictRef, (const void **) keysTypeRef, (const void **) valuesRef);

		for (CFIndex i = 0; i < size; i++)
		{
			string c_key = cfstr_2_cstr_utf8((CFStringRef)keysTypeRef[i]);//iphone, ipad. and so on.

			int t=CFGetTypeID(valuesRef[i]);

			if (t == CFDictionaryGetTypeID())
			{
				CFShow(valuesRef[i]);
				CFIndex sizei = CFDictionaryGetCount((CFDictionaryRef)valuesRef[i]);
				CFTypeRef *keysTypeRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
				CFTypeRef *valuesRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
				if (keysTypeRefi && valuesRefi)
				{
					CFDictionaryGetKeysAndValues((CFDictionaryRef)valuesRef[i], (const void **) keysTypeRefi, (const void **) valuesRefi);

					for (CFIndex ii = 0; ii < sizei; ii++)
					{
						string c_keyi = cfstr_2_cstr_utf8((CFStringRef)keysTypeRefi[ii]);//version

						int t=CFGetTypeID(valuesRefi[ii]);

						if (t == CFDictionaryGetTypeID())
						{
							CFShow(valuesRefi[ii]);
							InfoCarrier	incar;
							if(ReadBundleInfo((CFDictionaryRef)valuesRefi[ii],incar))
							{
								ccfig.addProductInfo(c_key, c_keyi, incar);
							}
						}
					}

				}
				if (keysTypeRefi)
				{
					free(keysTypeRefi);
				}
				if (valuesRefi)
				{
					free(valuesRefi);
				}

			}
		}

	}
	if (keysTypeRef)
	{
		free(keysTypeRef);
	}
	if (valuesRef)
	{
		free(valuesRef);
	}

}

//void ParserByProductType(CFDictionaryRef dictRef, CarrierConfig &ccfig)
//{
//	CFDictionaryRef dicA=NULL;
//	if (CFDictionaryContainsKey(dictRef, CFSTR("iPhone")))
//	{
//		dicA = (CFDictionaryRef)CFDictionaryGetValue(dictRef, CFSTR("iPhone"));
//		CFIndex sizei = CFDictionaryGetCount(dicA);
//		CFTypeRef *keysTypeRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
//		CFTypeRef *valuesRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
//		if (keysTypeRefi && valuesRefi)
//		{
//			CFDictionaryGetKeysAndValues(dicA, (const void **) keysTypeRefi, (const void **) valuesRefi);
//
//			for (CFIndex ii = 0; ii < sizei; ii++)
//			{
//				string c_keyi = cfstr_2_cstr_utf8((CFStringRef)keysTypeRefi[ii]);//version
//
//				int t=CFGetTypeID(valuesRefi[ii]);
//
//				if (t == CFDictionaryGetTypeID())
//				{
//					CFShow(valuesRefi[ii]);
//					InfoCarrier	incar;
//					ReadBundleInfo((CFDictionaryRef)valuesRefi[ii],incar);
//					ccfig.addiPhoneInfo(c_keyi, incar);
//				}
//			}
//
//		}
//		if (keysTypeRefi)
//		{
//			free(keysTypeRefi);
//		}
//		if (valuesRefi)
//		{
//			free(valuesRefi);
//		}
//
//	}
//	if (CFDictionaryContainsKey(dictRef, CFSTR("iPad")))
//	{
//		dicA = (CFDictionaryRef)CFDictionaryGetValue(dictRef, CFSTR("iPad"));
//		CFIndex sizei = CFDictionaryGetCount(dicA);
//		CFTypeRef *keysTypeRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
//		CFTypeRef *valuesRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
//		if (keysTypeRefi && valuesRefi)
//		{
//			CFDictionaryGetKeysAndValues(dicA, (const void **) keysTypeRefi, (const void **) valuesRefi);
//
//			for (CFIndex ii = 0; ii < sizei; ii++)
//			{
//				string c_keyi = cfstr_2_cstr_utf8((CFStringRef)keysTypeRefi[ii]);//version
//
//				int t=CFGetTypeID(valuesRefi[ii]);
//
//				if (t == CFDictionaryGetTypeID())
//				{
//					CFShow(valuesRefi[ii]);
//					InfoCarrier	incar;
//					ReadBundleInfo((CFDictionaryRef)valuesRefi[ii],incar);
//					ccfig.addiPadInfo(c_keyi, incar);
//				}
//			}
//
//		}
//		if (keysTypeRefi)
//		{
//			free(keysTypeRefi);
//		}
//		if (valuesRefi)
//		{
//			free(valuesRefi);
//		}
//	}
//
//}

void GetXMLBundleIPCCFiles()
{
	
	std::map<std::string,CarrierConfig>::iterator it;
	for(it=g_carrierbunders.begin();it!=g_carrierbunders.end();++it)
	{
		string sCarrier = it->first;
		CarrierConfig carconfig = it->second;
		map<string, InfoCarrier>::iterator iti;
		map<string,map<string, InfoCarrier>>::iterator ittype;
		for(iti=carconfig.m_gernalInfo.begin();iti!=carconfig.m_gernalInfo.end();++iti)
		{
			string sosverion = iti->first;
			InfoCarrier info = iti->second;
			char sPATH[MAX_PATH]={0};
			sprintf_s(sPATH,"%s\\%s\\%s", sCarrier.c_str(), sosverion.c_str(), info.bundleVersion.c_str());
			DownloadFile(info.sBundleURL.c_str(),sPATH);
		}
		//for(iti=carconfig.m_iPhoneInfo.begin();iti!=carconfig.m_iPhoneInfo.end();++iti)
		//{
		//	string sosverion = iti->first;
		//	InfoCarrier info = iti->second;
		//	char sPATH[MAX_PATH]={0};
		//	sprintf_s(sPATH,"%s\\iPhone\\%s\\%s", sCarrier.c_str(), sosverion.c_str(), info.bundleVersion.c_str());
		//	DownloadFile(info.sBundleURL.c_str(),sPATH);	
		//}
		//for(iti=carconfig.m_iPadInfo.begin();iti!=carconfig.m_iPadInfo.end();++iti)
		//{
		//	string sosverion = iti->first;
		//	InfoCarrier info = iti->second;
		//	char sPATH[MAX_PATH]={0};
		//	sprintf_s(sPATH,"%s\\iPad\\%s\\%s", sCarrier.c_str(), sosverion.c_str(), info.bundleVersion.c_str());
		//	DownloadFile(info.sBundleURL.c_str(), sPATH);
		//}
		for (ittype = carconfig.m_byproducttype.begin(); ittype!=carconfig.m_byproducttype.end(); ++ittype)
		{
			string stype = ittype->first;
			for(iti=ittype->second.begin();iti!=ittype->second.end();++iti)
			{
				string sosverion = iti->first;
				InfoCarrier info = iti->second;
				char sPATH[MAX_PATH]={0};
				sprintf_s(sPATH,"%s\\%s\\%s\\%s", sCarrier.c_str(),stype.c_str(), sosverion.c_str(), info.bundleVersion.c_str());
				DownloadFile(info.sBundleURL.c_str(), sPATH);
			}
		}
	}
}

void PlistFileToCarrierFile(const char *plst, int nLen)
{
	if(plst != NULL && nLen > 0)
	{
		CFDataRef plstdata = CFDataCreate (NULL, (const UInt8 *)plst, nLen);
		if (plstdata!=NULL)
		{
			CFStringRef error = NULL;
			CFPropertyListRef plistref = CFPropertyListCreateFromXMLData(kCFAllocatorDefault, plstdata, kCFPropertyListImmutable, &error);
			if (error) {
				logItC(_T("creating property list failed\n"));
			}
			const void* pMDCBBPV;// = CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("MobileDeviceCarriers"));
			//logItC(_T("MobileDeviceCarriers:\n"));
			//if (pMDCBBPV && CFGetTypeID(pMDCBBPV) == CFDictionaryGetTypeID())
			//{
			//	CFIndex size = CFDictionaryGetCount((CFDictionaryRef)pMDCBBPV);
			//	CFTypeRef *keysTypeRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
			//	CFTypeRef *valuesRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
			//	if (keysTypeRef && valuesRef)
			//	{
			//		CFDictionaryGetKeysAndValues((CFDictionaryRef)pMDCBBPV, (const void **) keysTypeRef, (const void **) valuesRef);
			//		for (CFIndex i = 0; i < size; i++)
			//		{
			//			string c_key = cfstr_2_cstr_utf8((CFStringRef)keysTypeRef[i]);//iccid
			//			string c_value = cfstr_2_cstr_utf8((CFStringRef)valuesRef[i]);//carrier
			//			logItC(_T("%S=%S\n"), c_key.c_str(), c_value.c_str());
			//		}
			//	}
			//	if (keysTypeRef)
			//	{
			//		free(keysTypeRef);
			//	}
			//	if (valuesRef)
			//	{
			//		free(valuesRef);
			//	}
			//}

			//pMDCBBPV = CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("MobileDeviceCarriersByCarrierID"));
			//logItC(_T("MobileDeviceCarriersByCarrierID:\n"));

			//if (pMDCBBPV && CFGetTypeID(pMDCBBPV) == CFDictionaryGetTypeID())
			//{
			//	CFIndex size = CFDictionaryGetCount((CFDictionaryRef)pMDCBBPV);
			//	CFTypeRef *keysTypeRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
			//	CFTypeRef *valuesRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
			//	if (keysTypeRef && valuesRef)
			//	{
			//		CFDictionaryGetKeysAndValues((CFDictionaryRef)pMDCBBPV, (const void **) keysTypeRef, (const void **) valuesRef);
			//		for (CFIndex i = 0; i < size; i++)
			//		{
			//			string c_key = cfstr_2_cstr_utf8((CFStringRef)keysTypeRef[i]);//iccid
			//			string c_value = cfstr_2_cstr_utf8((CFStringRef)valuesRef[i]);//carrier
			//			logItC(_T("%S=%S\n"), c_key.c_str(), c_value.c_str());
			//		}
			//	}
			//	if (keysTypeRef)
			//	{
			//		free(keysTypeRef);
			//	}
			//	if (valuesRef)
			//	{
			//		free(valuesRef);
			//	}
			//}

			pMDCBBPV = CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("MobileDeviceCarriersByMccMnc"));
			logItC(_T("MobileDeviceCarriersByMccMnc:\n"));

			if (pMDCBBPV && CFGetTypeID(pMDCBBPV) == CFDictionaryGetTypeID())
			{
				CFIndex size = CFDictionaryGetCount((CFDictionaryRef)pMDCBBPV);
				CFTypeRef *keysTypeRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
				CFTypeRef *valuesRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
				if (keysTypeRef && valuesRef)
				{
					CFDictionaryGetKeysAndValues((CFDictionaryRef)pMDCBBPV, (const void **) keysTypeRef, (const void **) valuesRef);
					for (CFIndex i = 0; i < size; i++)
					{
						//string c_key = cfstr_2_cstr_utf8((CFStringRef)keysTypeRef[i]);//iccid
						int t=CFGetTypeID(valuesRef[i]);

						if (t == CFDictionaryGetTypeID())
						{
							if (CFDictionaryContainsKey((CFDictionaryRef)valuesRef[i], CFSTR("MVNOs")))
							{
								const void* ppmvnos = CFDictionaryGetValue((CFDictionaryRef)valuesRef[i], CFSTR("MVNOs"));

								CFIndex ppmvnossize = CFArrayGetCount((CFArrayRef)ppmvnos);
								for (CFIndex ii = 0; ii < ppmvnossize; ii++)
								{
									const void *valuesppmvnos = CFArrayGetValueAtIndex((CFArrayRef)ppmvnos, ii); 
									t = CFGetTypeID(valuesppmvnos);
									if(t == CFDictionaryGetTypeID())
									{
										CFDictionaryRef iccidlist = (CFDictionaryRef)valuesppmvnos;
										if (CFDictionaryContainsKey((CFDictionaryRef)iccidlist, CFSTR("ICCID")) && CFDictionaryContainsKey((CFDictionaryRef)iccidlist, CFSTR("BundleName")))
										{
											string c_key = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue((CFDictionaryRef)iccidlist, CFSTR("ICCID")));//iccid
											string c_value = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue((CFDictionaryRef)iccidlist, CFSTR("BundleName")));//carrier
											logItC(_T("%S=%S\n"), c_key.c_str(), c_value.c_str());

										}
										else
											CFShow(iccidlist);
									}
								}

							}
						}
					}
				}
				if (keysTypeRef)
				{
					free(keysTypeRef);
				}
				if (valuesRef)
				{
					free(valuesRef);
				}
			}

			CFRelease(plstdata);
			CFRelease(plistref);
		}

	}

}

void PlistFileParser(const char *plst, int nLen)
{
	if(plst != NULL && nLen > 0)
	{
		CFDataRef plstdata = CFDataCreate (NULL, (const UInt8 *)plst, nLen);
		if (plstdata!=NULL)
		{
			CFStringRef error = NULL;
			CFPropertyListRef plistref = CFPropertyListCreateFromXMLData(kCFAllocatorDefault, plstdata, kCFPropertyListImmutable, &error);
			if (error) {
				logItC(_T("creating property list failed\n"));
			}
			const void* pMDCBBPV = CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("MobileDeviceCarrierBundlesByProductVersion"));
			if (pMDCBBPV && CFGetTypeID(pMDCBBPV) == CFDictionaryGetTypeID())
			{
				CFIndex size = CFDictionaryGetCount((CFDictionaryRef)pMDCBBPV);
				CFTypeRef *keysTypeRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
				CFTypeRef *valuesRef = (CFTypeRef *) malloc( size * sizeof(CFTypeRef) );
				if (keysTypeRef && valuesRef)
				{
					CFDictionaryGetKeysAndValues((CFDictionaryRef)pMDCBBPV, (const void **) keysTypeRef, (const void **) valuesRef);

					for (CFIndex i = 0; i < size; i++)
					{
						string c_key = cfstr_2_cstr_utf8((CFStringRef)keysTypeRef[i]);//carrier
						CarrierConfig ccfig;
						int t=CFGetTypeID(valuesRef[i]);

						if (t == CFDictionaryGetTypeID())
						{
							CFIndex sizei = CFDictionaryGetCount((CFDictionaryRef)valuesRef[i]);
							CFTypeRef *keysTypeRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
							CFTypeRef *valuesRefi = (CFTypeRef *) malloc( sizei * sizeof(CFTypeRef) );
							if (keysTypeRefi && valuesRefi)
							{
								CFDictionaryGetKeysAndValues((CFDictionaryRef)valuesRef[i], (const void **) keysTypeRefi, (const void **) valuesRefi);

								for (CFIndex ii = 0; ii < sizei; ii++)
								{
									if (CFEqual(keysTypeRefi[ii], CFSTR("ByProductType")))
									{
										CFShow(valuesRefi[ii]);
										//ParserByProductType((CFDictionaryRef)valuesRefi[ii], ccfig);
										ParserByProductType_v1((CFDictionaryRef)valuesRefi[ii], ccfig);
									}
									else
									{
										string c_keyi = cfstr_2_cstr_utf8((CFStringRef)keysTypeRefi[ii]);//version

										int t=CFGetTypeID(valuesRefi[ii]);

										if (t == CFDictionaryGetTypeID())
										{
											CFShow(valuesRefi[ii]);
											InfoCarrier	incar;
											ReadBundleInfo((CFDictionaryRef)valuesRefi[ii],incar);
											ccfig.addGernalInfo(c_keyi, incar);
										}
									}

								}

							}
							if (keysTypeRefi)
							{
								free(keysTypeRefi);
							}
							if (valuesRefi)
							{
								free(valuesRefi);
							}

						}
						g_carrierbunders.insert(pair<string, CarrierConfig>(c_key, ccfig));
					}

				}
				if (keysTypeRef)
				{
					free(keysTypeRef);
				}
				if (valuesRef)
				{
					free(valuesRef);
				}
			}
			CFRelease(plstdata);
		}

	}
	GetXMLBundleIPCCFiles();
}

std::string GetPlistInfoName(const char* fileName)
{
	CFPropertyListRef plistref;
	CFStringRef       errorString;
	CFDataRef         resourceData;
	BOOL           status;
	SInt32            errorCode;

	string sNameCarrier;
	CFURLRef fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, __CFStringMakeConstantString(fileName),  kCFURLWindowsPathStyle, false );
	status = CFURLCreateDataAndPropertiesFromResource(
		kCFAllocatorDefault,
		fileURL,
		&resourceData,            // place to put file data
		NULL,
		NULL,
		&errorCode);

	// Reconstitute the dictionary using the XML data.
	plistref = CFPropertyListCreateFromXMLData( kCFAllocatorDefault,
		resourceData,
		kCFPropertyListImmutable,
		&errorString);

	CFShow(plistref);
	CFDictionaryRef dict = (CFDictionaryRef)plistref;
	if(CFDictionaryContainsKey(dict,CFSTR("CFBundleIdentifier")))
	{
		sNameCarrier = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue(dict, CFSTR("CFBundleIdentifier")));
		sNameCarrier.append("=");
		sNameCarrier.append(cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue(dict, CFSTR("CFBundleExecutable"))));
	}

	if (resourceData) {
		CFRelease( resourceData );
	}else {
		CFRelease( errorString );
	}
	return sNameCarrier;
}


std::string GetPlistCarriarName(const char* fileName)
{
	CFPropertyListRef plistref;
	CFStringRef       errorString;
	CFDataRef         resourceData;
	BOOL           status;
	SInt32            errorCode;

	string sNameCarrier;
	CFURLRef fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, __CFStringMakeConstantString(fileName),  kCFURLWindowsPathStyle, false );
	status = CFURLCreateDataAndPropertiesFromResource(
		kCFAllocatorDefault,
		fileURL,
		&resourceData,            // place to put file data
		NULL,
		NULL,
		&errorCode);

	// Reconstitute the dictionary using the XML data.
	plistref = CFPropertyListCreateFromXMLData( kCFAllocatorDefault,
		resourceData,
		kCFPropertyListImmutable,
		&errorString);

	CFShow(plistref);
	CFDictionaryRef dict = (CFDictionaryRef)plistref;
	if(CFDictionaryContainsKey(dict,CFSTR("CarrierName")))
	{
		sNameCarrier = cfstr_2_cstr_utf8((CFStringRef)CFDictionaryGetValue(dict, CFSTR("CarrierName")));
	}

	if (resourceData) {
		CFRelease( resourceData );
	}else {
		CFRelease( errorString );
	}
	return sNameCarrier;
}

std::wstring UTF16FromUTF8(const char * utf8) 
{ 
	// 
	// Special case of empty input string 
	// 
	if (utf8 == NULL || *utf8 == '\0') 
		return std::wstring(); 


	// Prefetch the length of the input UTF-8 string 
	const int utf8Length = static_cast<int>(strlen(utf8)); 

	// Fail if an invalid input character is encountered 
	const DWORD conversionFlags = MB_ERR_INVALID_CHARS; 

	// 
	// Get length (in wchar_t's) of resulting UTF-16 string 
	// 
	const int utf16Length = ::MultiByteToWideChar( 
		CP_UTF8,            // convert from UTF-8 
		conversionFlags,    // flags 
		utf8,               // source UTF-8 string 
		utf8Length,         // length (in chars) of source UTF-8 string 
		NULL,               // unused - no conversion done in this step 
		0                   // request size of destination buffer, in wchar_t's 
		); 
	if (utf16Length == 0) 
	{ 
		// Error 
		DWORD error = ::GetLastError(); 
	} 


	// 
	// Allocate destination buffer for UTF-16 string 
	// 
	std::wstring utf16; 
	utf16.resize(utf16Length); 


	// 
	// Do the conversion from UTF-8 to UTF-16 
	// 
	if ( ! ::MultiByteToWideChar( 
		CP_UTF8,            // convert from UTF-8 
		0,                  // validation was done in previous call,  
		// so speed up things with default flags 
		utf8,               // source UTF-8 string 
		utf8Length,         // length (in chars) of source UTF-8 string 
		&utf16[0],          // destination buffer 
		utf16.length()      // size of destination buffer, in wchar_t's 
		) ) 
	{ 
		// Error 
		DWORD error = ::GetLastError(); 
		
	} 


	// 
	// Return resulting UTF-16 string 
	// 
	return utf16; 
} 